import java.util.*;
import java.math.BigInteger;
// I am using String as a datatype in all the Methods!

public class DESProcessor {
	// Constants!
	private static int[] INITIAL_PERMUTATION_TABLE = {
        58, 50, 42, 34, 26, 18, 10, 2,
        60, 52, 44, 36, 28, 20, 12, 4,
        62, 54, 46, 38, 30, 22, 14, 6,
        64, 56, 48, 40, 32, 24, 16, 8,
        57, 49, 41, 33, 25, 17, 9, 1,
        59, 51, 43, 35, 27, 19, 11, 3,
        61, 53, 45, 37, 29, 21, 13, 5,
        63, 55, 47, 39, 31, 23, 15, 7
    };

    private static int[] E_BIT_EXPANSION = {
    	32,  1,  2,  3,  4,  5,
    	4,   5,  6,  7,  8,  9,
    	8,   9,  10, 11, 12, 13,
    	12,  13, 14, 15, 16, 17,
    	16,  17, 18, 19, 20, 21,
    	20,  21, 22, 23, 24, 25,
    	24,  25, 26, 27, 28, 29,
    	28,  29, 30, 31, 32, 1
    };

    private static int[]  PC1_TABLE = {
        57, 49, 41, 33, 25, 17, 9, 1,
        58, 50, 42, 34, 26, 18, 10, 2,
        59, 51, 43, 35, 27, 19, 11, 3,
        60, 52, 44, 36, 63, 55, 47, 39,
        31, 23, 15, 7, 62, 54, 46, 38,
        30, 22, 14, 6, 61, 53, 45, 37,
        29, 21, 13, 5, 28, 20, 12, 4
	};

	private static int[] PC2_TABLE = {
        14, 17, 11, 24, 1, 5,
        3,  28, 15, 6,  21, 10,
        23, 19, 12, 4,  26, 8,
        16, 7,  27, 20, 13, 2,
        41, 52, 31, 37, 47, 55,
        30, 40, 51, 45, 33, 48,
        44, 49, 39, 56, 34, 53,
        46, 42, 50, 36, 29,  32
    };

	private static int[] LEFT_CIRCULAR_SHIFT_TABLE = {
        1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1
    };

    private static int[][][] S_BOXES = {
    	{
            {14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7},
		{0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8},
		{4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0},
		{15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13},

        }, // s1
    	{
           {15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},
		{3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},
		{0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},
		{13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}
        }, // s2
        {
		   {10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8},
		{13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1},
		{13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7},
		{1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}

		}, // s3
		{
		   {7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},
		{13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},
		{10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},
		{3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}

		},	 // s4
		{
		    {2,12,4,1,7,10,11,6,8,5,3,15,13,0,14,9},
		{14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6},
		{4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14},
		{11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}

		}, // s5
		{
			{12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11},
		{10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8},
		{9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6},
		{4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}

		}, //s-6
		{
		    {4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1},
		{13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6},
		{1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2},
		{6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}

		}, // s7
		{
		   {13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7},
		{1,15,13,8,10,3,7,4,12,5,6,11,0,14,9,2},
		{7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8},
		{2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}
		} //s-8
    };

    private static int [] FINAL_PERMUTATION_CONSTANT = {
		16,7,20,21,
		29,12,28,17,
		1,15,23,26,
		5,18,31,10,
		2,8,24,14,
		32,27,3,9,
		19,13,30,6,
		22,11,4,25
	};

	private static int[] INVERSE_INITIAL_PERMUTATION = {
		40,8,48,16,56,24,64,32,
		39,7,47,15,55,23,63,31,
		38,6,46,14,54,22,62,30,
		37,5,45,13,53,21,61,29,
		36,4,44,12,52,20,60,28,
		35,3,43,11,51,19,59,27,
		34,2,42,10,50,18,58,26,
		33,1,41,9,49,17,57,25
	};
	
	private static String hexToBinary(String hex) {
	    BigInteger bigInteger = new BigInteger(hex, 16);
	    String binaryString = bigInteger.toString(2);
	    int padding = hex.length() * 4 - binaryString.length();
	    if (padding > 0) {
	        binaryString = "0".repeat(padding) + binaryString  ;
	    }
    	return binaryString;
	}

	private static String binaryToHex(String binaryString) {
	    BigInteger bigInteger = new BigInteger(binaryString, 2);
	    String hexString = bigInteger.toString(16);
	    int padding = (int) Math.ceil(binaryString.length() / 4.0) - hexString.length();
	    if (padding > 0) {
	        hexString = "0".repeat(padding) + hexString;
	    }
	    return hexString.toUpperCase();
	}

	private static String leftCircularShift(String pc1Key, int shiftAmount) {
    	int length = pc1Key.length();
    	String leftCircularShifted = pc1Key.substring(shiftAmount) + 
    						pc1Key.substring(0, shiftAmount);
    	return leftCircularShifted;
    }
	private static String performPermutation(String input, int[] permutationArray) {
		StringBuilder permutedResult = new StringBuilder();

		for(int i: permutationArray) {
			permutedResult.append(input.charAt(i-1));
		}
		return permutedResult.toString();
	}
	private static String xorWithKey(String expandedRightHalfMessage, String roundKey) {
    	StringBuilder xorResult = new StringBuilder();

    	for( int i=0; i<expandedRightHalfMessage.length(); i++ ) {
    		char bit1 = expandedRightHalfMessage.charAt(i);
    		char bit2 = roundKey.charAt(i%roundKey.length()); // it was giving me exception while doing with left one

    		char xorBit = (bit1 == bit2) ? '0' : '1';

    		xorResult.append(xorBit);
    	}
    	return xorResult.toString();
    }
	private static String substitutionChoice(String input) {
    	StringBuilder substitutedResult = new StringBuilder(); 

    	for( int i=0; i<48; i+=6 ){
    		String sixBits = input.substring(i,i+6);
    		int row = Integer.parseInt(sixBits.charAt(0) + "" + sixBits.charAt(5),2);
    		int col = Integer.parseInt(sixBits.substring(1,5),2); // we want in binary thats why radix as 2
    		// see i want which box to consider while substituting 

    		int boxIndex = i/6;
    		int value = S_BOXES[boxIndex][row][col];
    		// System.out.println(value);
    		
    		String formattedBinary = String.format("%4s" , Integer.toBinaryString(value)).replace(' ', '0');
    		substitutedResult.append(formattedBinary);
    	}
    	return substitutedResult.toString();
    }

	private static String DES(String hexText, ArrayList<String> subkeys) {
		String binaryText = hexToBinary(hexText);

		String ipString = performPermutation(binaryText,INITIAL_PERMUTATION_TABLE);
		String leftHalfMessage = ipString.substring(0,32);
		String rightHalfMessage = ipString.substring(32);

		for( String key:  subkeys ) {
			// For Message
			String expandedRightHalfMessage = performPermutation(rightHalfMessage,E_BIT_EXPANSION);
			// System.out.println(expandedRightHalfMessage); // correct

			String xorResult = xorWithKey(expandedRightHalfMessage,key);
			// System.out.println("Round: " + round + " key: " + xorResult); // correct 

			String sboxResult;
			sboxResult = substitutionChoice(xorResult);
			// System.out.println("Round: " + " key: " + sboxResult); // correct

			String finalPermutedResult = performPermutation(sboxResult,FINAL_PERMUTATION_CONSTANT);
			// System.out.println("Final Permuation result: " + finalPermutedResult);

			String afterPermutationXORResult = xorWithKey(leftHalfMessage, finalPermutedResult);
			// System.out.println("XOR result with left half: " + afterPermutationXORResult);

			leftHalfMessage = rightHalfMessage;
			rightHalfMessage = afterPermutationXORResult;
		}
		String temp = leftHalfMessage;
		leftHalfMessage = rightHalfMessage;
		rightHalfMessage = temp;

		String finalMessage = leftHalfMessage + rightHalfMessage;
		String inversePermutationResult = performPermutation(finalMessage,INVERSE_INITIAL_PERMUTATION );
		return inversePermutationResult;	
	}
	private static ArrayList<String> getSubkeys(String hexKey) {
		ArrayList<String> subkeys = new ArrayList<>();
		String binaryKey = hexToBinary(hexKey);
        String pc1Key = performPermutation(binaryKey, PC1_TABLE);
        String leftHalfKey = pc1Key.substring(0, 28);
        String rightHalfKey = pc1Key.substring(28);

        for (int round = 1; round <= 16; round++) {
            leftHalfKey = leftCircularShift(leftHalfKey, LEFT_CIRCULAR_SHIFT_TABLE[round - 1]);
            rightHalfKey = leftCircularShift(rightHalfKey, LEFT_CIRCULAR_SHIFT_TABLE[round - 1]);

            String roundKey = leftHalfKey + rightHalfKey;
            roundKey = performPermutation(roundKey, PC2_TABLE);
            subkeys.add(roundKey);
        }

        return subkeys;
    }
	public static String getEncryptedResult(String hexText,String hexKey) {
		ArrayList<String> subkeys = getSubkeys(hexKey);
		// System.out.println(subkeys);
		String inversePermutationResult = DES(hexText,subkeys);
		return binaryToHex(inversePermutationResult);
	}

	public static String getDecryptedResult(String hexText, String hexKey) {
		ArrayList<String> subkeys = getSubkeys(hexKey);
		Collections.reverse(subkeys);
		// System.out.println("                                  " + subkeys);
        String inversePermutationResult = DES(hexText,subkeys);
        System.out.println(inversePermutationResult);
        return binaryToHex(inversePermutationResult);
    }
	public static void main(String[] args) {
		// String hexText = "0123456789ABCDEF";
		String hexText = args[0] ;
		String hexKey = args[1];

		// DESProcessor des = new DESProcessor();
		String encryptedResult = DESProcessor.getEncryptedResult(hexText, hexKey);

		String decryptedResult = DESProcessor.getDecryptedResult(encryptedResult, hexKey);

		System.out.println("Encrypted Result: " + encryptedResult);
		System.out.println("Decrypted Result: " + decryptedResult);
	}	

}